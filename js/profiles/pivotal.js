(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  (function() {
    var PivotalProfile;
    PivotalProfile = (function() {
      function PivotalProfile(config) {
        this.config = config;
        this.addTimer = __bind(this.addTimer, this);
        this.projectNameSelector = "header.project section.name h2 a";
        this.storyNameSelector = '#maximizes_show textarea[name="story[name]"]';
        // this.actionSelector = "#maximizes_show .story.info";
        this.actionSelector = "#maximizes_show .container.point_scale_custom";
        this.platformLoaded = false;
        this.loadHarvestPlatform();
        this.addTimerWhenUrlChanges();
      }

      PivotalProfile.prototype.loadHarvestPlatform = function() {
        var configScript, ph, platformConfig, platformScript,
          _this = this;
        platformConfig = {
          applicationName: "Pivotal",          
          permalink: "https://www.pivotaltracker.com/s/projects/%PROJECT_ID%/stories/%ITEM_ID%",
          environment: this.config.environment,
          skipStyling: true
        };
        configScript = document.createElement("script");
        configScript.innerHTML = "window._harvestPlatformConfig = " + (JSON.stringify(platformConfig)) + ";";
        platformScript = document.createElement("script");
        platformScript.src = "//" + this.config.subdomain + this.config.domain + "/assets/platform.js";
        platformScript.async = true;
        ph = document.getElementsByTagName("script")[0];
        ph.parentNode.insertBefore(configScript, ph);
        ph.parentNode.insertBefore(platformScript, ph);
        return document.body.addEventListener("harvest-event:ready", function() {
          _this.platformLoaded = true;  
          setTimeout(function(){
            _this.addTimer();
          }, 1000)
          return;
        });          
      };

      PivotalProfile.prototype.addTimer = function() {
        var data;
        if (!this.platformLoaded) {
          return;
        }
        if (document.querySelector(".harvest-timer") != null) {
          return;
        }
        data = this.getDataForTimer();
        if (this.notEnoughInfo(data)) {
          return;
        }
        this.buildTimer(data);
        return this.notifyPlatformOfNewTimers();
      };

      PivotalProfile.prototype.getDataForTimer = function() {
        var itemName, link, linkParts, projectName, _ref, _ref1;
        itemName = (_ref = document.querySelector(this.storyNameSelector)) != null ? _ref.value.trim() : void 0;
        projectName = (_ref1 = document.querySelector(this.projectNameSelector)) != null ? _ref1.innerText.trim() : void 0;
        link = window.location.href;
        linkParts = link.match(/^https:\/\/www.pivotaltracker.com\/s\/projects\/([a-zA-Z0-9-]+)\/stories\/([a-zA-Z0-9-]+)$/);
        itemID = linkParts != null ? linkParts[2] : void 0
        itemName = "[#"+itemID+"] " + itemName;
        data =  {
          project: {
            id: linkParts != null ? linkParts[1] : void 0,
            name: projectName
          },
          item: {
            id: itemID,
            name: itemName
          }
        };
        return data;
      };

      PivotalProfile.prototype.notEnoughInfo = function(data) {
        var _ref, _ref1;
        return !(((data != null ? (_ref = data.project) != null ? _ref.id : void 0 : void 0) != null) && ((data != null ? (_ref1 = data.item) != null ? _ref1.id : void 0 : void 0) != null));
      };

      PivotalProfile.prototype.buildTimer = function(data) {
        var actions, icon, timer;
        actions = document.querySelector(this.actionSelector);
        if (!actions) {
          return;
        }
        
        container = document.createElement("div");
        container.className = "harvest-timer-container js-add-pivotal-timer-container";
        
        timer = document.createElement("a");
        timer.className = "harvest-timer button-link js-add-pivotal-timer";
        timer.setAttribute("id", "harvest-pivotal-timer");
        timer.setAttribute("data-project", JSON.stringify(data.project));
        timer.setAttribute("data-item", JSON.stringify(data.item));
        
        container.appendChild(timer);
        
        icon = document.createElement("span");
        icon.className = "pivotal-timer-icon";
        icon.style.backgroundImage = "url('" + (chrome.extension.getURL("images/pivotal-timer-icon.png")) + "')";
        
        timer.appendChild(icon);
        
        // timer.appendChild(document.createTextNode("Harvest Timer"));
        
        return actions.insertBefore(container, actions.children[0]);
      };

      PivotalProfile.prototype.notifyPlatformOfNewTimers = function() {
        var evt;
        evt = new CustomEvent("harvest-event:timers:chrome:add");
        return document.querySelector("#harvest-messaging").dispatchEvent(evt);
      };

      PivotalProfile.prototype.addTimerWhenUrlChanges = function() {
        var ph, script,
          _this = this;
        script = document.createElement("script");
        script.innerHTML = "(" + (this.notifyOnUrlChanges.toString()) + ")()";
        ph = document.getElementsByTagName("script")[0];
        ph.parentNode.insertBefore(script, ph);
        return window.addEventListener("message", function(evt) {
          if (evt.source !== window) {
            return;
          }
          if (evt.data !== "urlChange") {
            return;
          }
          return _this.addTimer();
        });
      };

      PivotalProfile.prototype.notifyOnUrlChanges = function() {
        var change, fn;
        change = function() {
          return window.postMessage("urlChange", "*");
        };
        fn = window.history.pushState;
        window.history.pushState = function() {
          fn.apply(window.history, arguments);
          return change();
        };
        return window.addEventListener("popstate", change);
      };

      return PivotalProfile;

    })();
    return chrome.runtime.sendMessage("retrieveConfig", function(config) {
      return new PivotalProfile(config);
    });
  })();

}).call(this);
